# Maintainer: Daniele Oriani <daniele.oriani.gm at gmail.com>

pkgname=st-git
_pkgname=st
pkgver=20160811.6e79e83
pkgrel=1
pkgdesc='Simple virtual terminal emulator for X'
url='http://git.suckless.org/st/'
arch=('i686' 'x86_64')
license=('MIT')
depends=('libxft')
makedepends=('ncurses' 'libxext' 'git')
groups=('custom')
patches=('http://st.suckless.org/patches/st-alpha-20160727-308bfbf.diff' \
         'http://st.suckless.org/patches/st-scrollback-0.7.diff' \
         'http://st.suckless.org/patches/st-scrollback-mouse-20161020-6e79e83.diff' \
         'st-colorscheme-gosper.patch')
source=("git://git.suckless.org/st" \
        "${patches[@]}")
sha1sums=('SKIP'
          '1e52187da4d8c7c153eca09f1edb8fd3d0abbf63'
          'f42d0b0ae25491792acbc6b3ffab025027957a13'
          '88b85b0f3dff3606c5c791ab5752fdfb36727c7c'
          'c3aa7d4dd6156781c914e1c571b869065848ec29')

provides=("${_pkgname}")
conflicts=("${_pkgname}")

pkgver() {
  cd "${srcdir}/${_pkgname}"
  git log -1 --format='%cd.%h' --date=short | tr -d -
}

prepare() {
  cd "${srcdir}/${_pkgname}"
  # For patch compatibility use the original v 0.7 commit
  git checkout -b "v0.7"  6e79e8357ed1987a7f7a52cc06249aadef478041
  local patchfile
  for patchfile in "${patches[@]/http*patches/}"
  do
    echo "Applying patch" ${patchfile}"..."
    patch -Np1 < "${srcdir}/${patchfile}"
  done
  sed \
          -e '/char font/s/= .*/= "Iosevka Term Slab:size=12:style=Regular";/' \
          -e '/char worddelimiters/s/= .*/= " '"'"'`\\\"()[]{}<>|";/' \
          -e '/int borderpx/s/= .*/= 3;/' \
          -e '/int tabspaces/s/= .*/= 2;/' \
          -e '/int alpha/s/= .*/= 0xf8;/' \
          -e '/int blinktimeout/s/= .*/= 0;/' \
          -e '/int defaultbg/s/= .*/= 0;/' \
          -e '/int defaultcs/s/= .*/= 7;/' \
          -e '/float cwscale/s/= .*/= 1.002;/' \
          -e '/char shell/s/= .*/= "\/usr\/bin\/zsh";/' \
          -i config.def.h
  sed \
          -e 's/CPPFLAGS =/CPPFLAGS +=/g' \
          -e 's/CFLAGS =/CFLAGS +=/g' \
          -e 's/LDFLAGS =/LDFLAGS +=/g' \
          -e 's/_BSD_SOURCE/_DEFAULT_SOURCE/' \
          -i config.mk
  sed '/@tic/d' -i Makefile
}

build() {
	cd "${srcdir}/${_pkgname}"
	make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
	cd "${srcdir}/${_pkgname}"
	make PREFIX=/usr DESTDIR="${pkgdir}" install
	install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
	install -Dm644 README "${pkgdir}/usr/share/doc/${pkgname}/README"
}
